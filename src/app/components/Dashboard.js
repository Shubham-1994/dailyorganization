import React from 'react';
import { connect } from 'react-redux';
import { ConnectedTask } from './TaskList';

export const Dashboard = ({ groups }) => (
  <div>
    {
      groups.map(group => (
        <div key={group.id}>
          <ConnectedTask id={group.id} name={group.name} />
        </div>
      ))
    }
  </div>
)

const mapStateToProps = (state) => {
  return {
    groups: state.groups
  }
}

export const ConnectedDashboard = connect(mapStateToProps)(Dashboard);
