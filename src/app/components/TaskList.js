import React from 'react';
import { connect } from 'react-redux';
import { createTask, requestTaskCreation } from '../store/actions/createTask';
const TaskList = ({ tasks, name, groupId, createNewTask }) => (
  <div>
    <h3>{name}</h3>
    {
      tasks.map(task => (
        <div key={task.id}>
          <div>{task.name}</div>
        </div>
      ))
    }
    <button onClick={() => createNewTask(groupId)}>Add new</button>
  </div>
)
const mapStateToProps = (state, ownProps) => {
  let groupId = ownProps.id;
  return {
    tasks: state.tasks.filter(task => task.group === groupId),
    name: ownProps.name,
    groupId
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    createNewTask: (groupId) => { dispatch(requestTaskCreation(groupId)) }
  }
}
export const ConnectedTask = connect(mapStateToProps, mapDispatchToProps)(TaskList);