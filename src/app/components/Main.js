import React from 'react';
import { store } from '../store/configureStore';
import { Provider } from 'react-redux';
import { ConnectedDashboard } from './Dashboard';
import { Router, Route } from 'react-router-dom';
import { history } from '../store/history';

export const Main = () => (
  <Router history={history}>
    <Provider store={store}>
      <Route exact path="/dashboard" render={() => (<ConnectedDashboard />)} />
      {/* <ConnectedDashboard /> */}
    </Provider>
  </Router>
)