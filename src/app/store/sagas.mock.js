import { take, select, put } from 'redux-saga/effects';

import * as actions from './actions/createTask';
import { v4 as uuidv4 } from 'uuid';

export function* taskCreationSaga() {
  while (true) {
    const { groupId } = yield take(actions.REQUEST_TASK_CREATION);
    const ownerId = 'U1';
    const taskId = uuidv4();
    yield put(actions.createTask(taskId, groupId, ownerId));
    console.log("got group id ", groupId);
  }
}

