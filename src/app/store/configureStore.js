import { createStore, applyMiddleware, compose } from 'redux';
import { reducer } from './reducers/taskReducer';
import { createLogger } from 'redux-logger';
import { taskCreationSaga } from './sagas.mock';
import createSagaMiddleware from 'redux-saga';


const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(reducer, composeEnhancers(applyMiddleware(createLogger(), sagaMiddleware)));


sagaMiddleware.run(taskCreationSaga);
// for (let saga in sagas) {
//   sagaMiddleware.run(saga);
// }