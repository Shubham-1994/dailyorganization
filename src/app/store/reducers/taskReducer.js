import { defaultState } from '../../../server/defaultState';
import * as actions from '../actions/createTask';

export function reducer(state = defaultState, action) {

  console.log(state, action.type, action.groupId)
  switch (action.type) {
    case actions.CREATE_TASK: {
      console.log("entered create task")
      let newtask = {
        name: "Learn React",
        id: action.taskId,
        group: action.groupId,
        owner: action.ownerId,
        isComplete: false,
      };

      return Object.assign({}, state, { tasks: [...state.tasks, newtask] });
    }
    default:
      return state;

  }

}